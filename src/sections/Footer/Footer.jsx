import './Footer.css'

export const Footer = () => {
  return (
    <footer>
      <div className='container'>
        <div className='row'>
          <div className='col-lg-12'>
            <p>Copyright © 2036 <a href="#">Waseem Gaming</a> Company. All rights reserved. </p>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer