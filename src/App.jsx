import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import "./App.css";

import { Header, Footer } from "./sections/index";

import { Home } from "./Pages/index";
import { Container } from "./components/index";

const App = () => {
  return (
    <>
      <Header />
      <Container>
        <Home />
      </Container>
      <Footer />
    </>
  );
};

export default App;
