import "./Card.css";

import {AiTwotoneStar} from 'react-icons/ai'
import {FiDownload} from 'react-icons/fi'


const Card = (props) => {
  return (
    <div className="most-popular-item">
      <div className="card-wrapper">
        <img className="most-popular-item-image" src={props.image} />
        <div className="most-popular-item-content">
          <h4 className="most-popular-item-title">
            {props.title} <br />
            <span>{props.category}</span>
          </h4>
          <ul>
            <li>
              <AiTwotoneStar className="star-icon" />
              <span>{props.rate}</span>
            </li>
            <li>
              <FiDownload className="download-icon" />
              <span>{props.download}</span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Card;
