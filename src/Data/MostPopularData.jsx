import popular_01 from "../assets/images/popular-01.jpg";
import popular_02 from "../assets/images/popular-02.jpg";
import popular_03 from "../assets/images/popular-03.jpg";
import popular_04 from "../assets/images/popular-04.jpg";

import popular_05 from "../assets/images/popular-05.jpg";
import popular_06 from "../assets/images/popular-06.jpg";
import popular_07 from "../assets/images/popular-07.jpg";
import popular_08 from "../assets/images/popular-08.jpg";


const MostPopularData = [
    {
        id: "most_popular_item_0", 
        image: popular_01, 
        title: "Fortnite", 
        category: "Sandbox", 
        rate: "3.3", 
        download: "3.4m"
    },
    {
        id: "most_popular_item_1", 
        image: popular_02, 
        title: "PubG", 
        category: "Stream-x", 
        rate: "4.6", 
        download: "3.4m"
    },
    {
        id: "most_popular_item_2", 
        image: popular_03, 
        title: "Dota2", 
        category: "Legendary", 
        rate: "4.0", 
        download: "2.4m"
    },
    {
        id: "most_popular_item_3", 
        image: popular_04, 
        title: "CS-Go", 
        category: "Battle 5", 
        rate: "4.1", 
        download: "2.6m"
    },
    {
        id: "most_popular_item_4", 
        image: popular_05, 
        title: "Mini Craft", 
        category: "Legendary", 
        rate: "4.7", 
        download: "3.0m"
    },
    {
        id: "most_popular_item_5", 
        image: popular_06, 
        title: "Eagles Fly", 
        category: "Matrix Games", 
        rate: "3.7", 
        download: "2.2m"
    },
    {
        id: "most_popular_item_6", 
        image: popular_07, 
        title: "Warface", 
        category: "Max 3D", 
        rate: "3.9", 
        download: "2.5m"
    },
    {
        id: "most_popular_item_7", 
        image: popular_08, 
        title: "Waecraft", 
        category: "Legend", 
        rate: "5.0", 
        download: "5.5m"
    },
]

export default MostPopularData