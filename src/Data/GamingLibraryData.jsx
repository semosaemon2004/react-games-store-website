import GamingLibrary_01 from "../assets/images/game-01.jpg";
import GamingLibrary_02 from "../assets/images/game-02.jpg";
import GamingLibrary_03 from "../assets/images/game-03.jpg";


const GamingLibraryData = [
    {
        id: "library_item_1", 
        image: GamingLibrary_01, 
        title: "Dota 2", 
        category: "Sandbox",
        date_added: "24/07/2024", 
        hours_played: "634 H 22 Mins", 
        downloaded: "Downloaded"
    },
    {
        id: "library_item_2", 
        image: GamingLibrary_02, 
        title: "Fortnite", 
        category: "Sandbox",
        date_added: "12/08/2024", 
        hours_played: "740 H 52 Mins", 
        downloaded: "Downloaded"
    },
    {
        id: "library_item_3", 
        image: GamingLibrary_03, 
        title: "CS-GO", 
        category: "Sandbox",
        date_added: "22/09/2024", 
        hours_played: "892 H 14 Mins", 
        downloaded: "Downloaded"
    },
    
]

export default GamingLibraryData